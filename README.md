# Redis Com Service

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_redis_com_service)

> This is an [AAAMBOS](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/) package. You can find more information about what AAAMBOS is [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/what_is_aaambos) and how to install it [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/).

A communication service based on the redis publish-subscribe functionality

## Installation
You need a running redis server. [Install redis](https://redis.io/docs/install/install-redis/).

## Usage
In your `arch_config.yml`:
```yaml
communication:
  communication_prefs:
    - !name:redis_com_service.communications.redis_com.RedisServiceInfo
```