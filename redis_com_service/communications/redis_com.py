import asyncio
from collections import defaultdict
from typing import Callable, Any, Awaitable, Type, Tuple

import msgspec
from aaambos.core.communication.promise import CommunicationPromise
from aaambos.core.communication.service import CommunicationService, CommunicationServiceInfo, to_attribute_dict
from aaambos.core.communication.setup import CommunicationSetup
from aaambos.core.communication.topic import Topic
from aaambos.core.configuration.module_config import ModuleCommunicationInfo, ModuleConfig
from aaambos.core.execution.concurrency import ConcurrencyLevel
from aaambos.std.communications.attributes import RunTimeSendingNewTopicsAttribute, RunTimeReceivingNewTopicsAttribute, \
    PayloadWrapperAttribute, MsgPayloadWrapperCallbackReceivingAttribute, MsgTopicSendingAttribute
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE, ONLY_LOCAL_TYPE

import redis.asyncio as redis
from loguru import logger
from redis.asyncio.client import Redis, PubSub

from redis_com_service.communications.definitions import REDIS_ADDRESS_KEY, REDIS_ADDRESS

REDIS_COM = "redis"
REDIS_COM_TYPE = "redis_com_type"
REDIS_IMPORT = "redis_com_service.communications.redis_com.RedisService"


async def reader(channel: PubSub, callbacks: dict[bytes, list[Tuple[Topic, Callable]]], wrapper: dict[bytes, Any]):
    while True:
        message = await channel.get_message(ignore_subscribe_messages=True)
        if message is not None:
            if message["channel"] in wrapper:
                data = msgspec.json.decode(message["data"], type=wrapper[message["channel"]])
            else:
                data = message["data"].decode("utf-8")
            if message["channel"] in callbacks:
                for callback in callbacks[message["channel"]]:
                    await callback[1](callback[0], data)


class RedisService(CommunicationService,
                   MsgTopicSendingAttribute,
                   MsgPayloadWrapperCallbackReceivingAttribute,
                   PayloadWrapperAttribute,
                   RunTimeReceivingNewTopicsAttribute,
                   RunTimeSendingNewTopicsAttribute):
    r: Redis
    pub_sub: PubSub | None

    def __init__(self, config: ModuleCommunicationInfo, address: str, in_promises: list[CommunicationPromise], out_promises: list[CommunicationPromise]):
        super().__init__(config)
        self.address = address
        self.in_promises = in_promises
        self.out_promises = out_promises
        self.callbacks = defaultdict(list)
        self.wrapper = {}
        self.pub_sub = None

    async def initialize(self):
        self.r = redis.from_url(self.address)
        in_topics = [in_promise.settings.topic.id() for in_promise in self.in_promises]
        if in_topics:
            self.pub_sub = self.r.pubsub()
            print(in_topics)
            await self.pub_sub.subscribe(*in_topics)
            logger.info(f"redis listen to {', '.join(in_topics)}")

            asyncio.create_task(reader(self.pub_sub, self.callbacks, self.wrapper))

    async def send(self, topic: Topic, msg):
        await self.r.publish(topic.id(), msgspec.json.encode(msg))

    async def register_callback(self, topic: Topic, callback: Callable, wrapper: Type[Any] = ..., *args, **kwargs):
        byte_topic_id = str.encode(topic.id())
        if byte_topic_id not in self.callbacks or (topic, callback) not in self.callbacks[byte_topic_id]:
            logger.info(f"Add callback {topic.id()}")
            self.callbacks[byte_topic_id].append((topic, callback))
            if wrapper is not ... and byte_topic_id not in self.wrapper:
                self.wrapper[byte_topic_id] = wrapper

    async def unregister_callback(self, topic: Topic, callback: Callable):
        if (topic, callback) in self.callbacks[str.encode(topic.id())]:
            self.callbacks[str.encode(topic.id())].remove((topic, callback))

    def check_wrapping(self, payload, wrapper: Type[Any]) -> bool:
        pass

    async def add_input_topic(self, topic: Topic, callback: Callable[[Topic, Any], Awaitable[None]] = ...,
                              wrapper: Any = ...):
        byte_topic_id = str.encode(topic.id())
        if byte_topic_id not in self.callbacks:
            logger.info(f"now also listen to {topic.id()}")
            self.callbacks[byte_topic_id].append((topic, callback))
            if wrapper is not ...:
                self.wrapper[byte_topic_id] = wrapper
            print(topic, topic.id())
            if not self.pub_sub:
                self.pub_sub = self.r.pubsub()
                await self.pub_sub.subscribe(topic.id())
                asyncio.create_task(reader(self.pub_sub, self.callbacks, self.wrapper))
            else:
                await self.pub_sub.subscribe(topic.id())

    async def add_output_topic(self, topic: Topic):
        pass


class RedisSetup(CommunicationSetup):
    def before_arch_start(self):
        pass

    def before_module_start(self, module_config: ModuleConfig) -> CommunicationService:
        in_proms, out_proms = self.communication_graph.get_in_out_promises_per_module(module_config.name, com_service_name=REDIS_COM)
        return RedisService(config=module_config.com_info, address=module_config.general_run_config.plus[REDIS_ADDRESS_KEY], in_promises=in_proms, out_promises=out_proms)

    @classmethod
    def run_config_general_plus_keys_and_defaults(cls) -> dict[str, Any]:
        return {REDIS_ADDRESS_KEY: REDIS_ADDRESS}


RedisServiceInfo = CommunicationServiceInfo(
    name=REDIS_COM,
    import_path=REDIS_IMPORT,
    attributes=to_attribute_dict([
        MsgTopicSendingAttribute,
        MsgPayloadWrapperCallbackReceivingAttribute,
        PayloadWrapperAttribute,
        RunTimeReceivingNewTopicsAttribute,
        RunTimeSendingNewTopicsAttribute
    ]),
    extra_categories=[REDIS_COM_TYPE, ConcurrencyLevel.MultiProcessing.name, MSGSPEC_STRUCT_TYPE, ONLY_LOCAL_TYPE],
    setup=RedisSetup,
)
