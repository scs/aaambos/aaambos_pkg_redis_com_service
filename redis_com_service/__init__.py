"""

This is the documentation of the aaambos package Redis Com Service.
It contains of an aaambos communication service,

# About the package

# Background / Literature

# Usage / Examples
## Installation
You need a running redis server. [Install redis](https://redis.io/docs/install/install-redis/).

## Usage
In your `arch_config.yml`:
```yaml
communication:
  communication_prefs:
    - !name:redis_com_service.communications.redis_com.RedisServiceInfo
```

# Citation


"""
